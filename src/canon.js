/*
###
### VARIABLES
###
 */

const context = new AudioContext();
const maxVoiceLength = 16; // 16 tones
const storage = window.localStorage;

let toneBuffers = {};
let voice1 = [];
let voice2 = [];
let toneArray = [];

// DOM
let loadTonesProgress = document.getElementById("loadTonesProgress");
let loadTonesText = document.getElementById("loadTonesText");

let playC3 = document.getElementById("playC3");
let playCs3 = document.getElementById("playCs3");
let playD3 = document.getElementById("playD3");
let playDs3 = document.getElementById("playDs3");
let playE3 = document.getElementById("playE3");
let playF3 = document.getElementById("playF3");
let playFs3 = document.getElementById("playFs3");
let playG3 = document.getElementById("playG3");
let playGs3 = document.getElementById("playGs3");
let playA3 = document.getElementById("playA3");
let playAs3 = document.getElementById("playAs3");
let playH3 = document.getElementById("playH3");
let playC4 = document.getElementById("playC4");
let playCs4 = document.getElementById("playCs4");
let playD4 = document.getElementById("playD4");
let playDs4 = document.getElementById("playDs4");
let playE4 = document.getElementById("playE4");
let playF4 = document.getElementById("playF4");
let playFs4 = document.getElementById("playFs4");
let playG4 = document.getElementById("playG4");
let playGs4 = document.getElementById("playGs4");
let playA4 = document.getElementById("playA4");
let playAs4 = document.getElementById("playAs4");
let playH4 = document.getElementById("playH4");
let playC5 = document.getElementById("playC5");

let playVoice1 = document.getElementById("playVoice1");
let resetVoice1 = document.getElementById("resetVoice1");
let undoVoice1 = document.getElementById("undoVoice1");
let saveVoice1 = document.getElementById("saveVoice1");
let loadVoice1 = document.getElementById("loadVoice1");
let removeVoice1 = document.getElementById("removeVoice1");
let memoryStatus = document.getElementById("memoryStatus");

let firstVoiceStatus = document.getElementById("firstVoiceStatus");
let secondVoiceStatus = document.getElementById("secondVoiceStatus");

let playVoice2 = document.getElementById("playVoice2");
let intervalSelect = document.getElementById("intervalSelect");
let shiftSelect = document.getElementById("shiftSelect");
let camden = document.getElementById("camden");

/*
###
### FUNCTIONS
###
 */

function loadTones() {
    let toneNames = ["C", "Cs", "D", "Ds", "E", "F", "Fs", "G", "Gs", "A", "As", "H"];

    for (let octave = 0; octave < 7; octave++) {                                    // a cycle so we don't have to load every sound manually
        for (let toneNumber = 0; toneNumber < toneNames.length; toneNumber++) {   // 7 octaves with 12 tones = 84 sound files

            let tone = toneNames[toneNumber] + octave;      // eg. "Ds2", "G6"
            loadTone(tone);
        }
    }
}

function loadTone(toneName) {
    let url = "../resource/piano/" + toneName + ".ogg";

    let request = new XMLHttpRequest();             // AJAX load of sound files so the page doesn't get stuck
    request.open("GET", url, true);
    request.responseType = "arraybuffer";           // AudioContext.decodeAudioData() expects an ArrayBuffer object

    request.onload = function() {
        context.decodeAudioData(request.response, function(buffer) {

            toneBuffers[toneName] = buffer;     // decodeAudioData returns an AudioBuffer

            loadTonesProgress.value = loadTonesProgress.value + (100 / 84);   // 84 tones to load

            if (loadTonesProgress.value >= 99) {    // it seems not to round to 100 after it's done, so we check 99

                loadTonesText.textContent = "All sounds loaded!";

                setTimeout(function() {
                    loadTonesProgress.style.display = "none";   // the progress bar has no function anymore, so we hide it after some time
                    loadTonesText.style.display = "none";
                }, 1000);
            }
        });
    };

    request.send();
}

function playTone(audioBuffer, beat) {

    let source = context.createBufferSource();  // after a source is started (sent to destination), it disappears and needs to be created again
    source.buffer = audioBuffer;
    source.connect(context.destination);
    source.start(beat, 0, 1.5);     // duration is 1.5s for a reverb effect when a melody is played
}

function playVoice(voice, shift) {
    let time = context.currentTime + 0.1 + shift;   // start playing after 0.1 s, add a beat shift. audioContext holds a property currentTime, which counts time after it has been started

    for (let beat = 0; beat < voice.length; beat++) {

        let buffer = toneBuffers[voice[beat]];
        playTone(buffer, beat + time);      // if we didn't add TIME to BEAT, every sound would fire immediately
    }
}

function playVoices(shift) {
    playVoice(voice1, 0);
    playVoice(voice2, shift);
}

function updateVoiceStatus() {
    firstVoiceStatus.innerHTML = "Notes in the melody: " + voice1;
    secondVoiceStatus.innerHTML = "Notes in the canon: " + voice2;
}

function transformVoice(semitones) {    // every tone can be shifted by an interval - interval is made of semitones
    if (toneArray.length === 0) {    // so we don't run this every time we want to play a canon, but only once

        let toneNames = ["C", "Cs", "D", "Ds", "E", "F", "Fs", "G", "Gs", "A", "As", "H"];

        for (let octave = 0; octave < 7; octave++) {
            for (let tone = 0; tone < toneNames.length; tone++) {   // a cycle so we don't have to fill the array manually
                toneArray.push(toneNames[tone] + octave);
            }
        }
    }

    for (let i = 0; i < voice1.length; i++) {   // lowest tone available to user - C3, highest - C5. lowest possible transformation - A1, highest - E6

        let tone = voice1[i];
        let toneIndex = toneArray.indexOf(tone);
        voice2[i] = toneArray[toneIndex + semitones];    // shift by a number of semitones and add it to voice2 = "tone algebra"
    }
}

function updateMemoryStatus() {
    memoryStatus.innerHTML = "Saved melodies: ";
    
    for (let i = 0; i < localStorage.length; i++) {
        let melody = document.createElement("A");
        melody.innerHTML = localStorage.key(i) + "<br>";

        melody.addEventListener("click", function() {

            let voice1Raw = storage.getItem(localStorage.key(i));
            let voice1Split = voice1Raw.split(",");
            voice1 = Array.from(voice1Split);
            voice2 = [];
            updateVoiceStatus();
        });

        memoryStatus.appendChild(melody);
    }
}



/*
###
### LOAD ALL SOUNDS
###
 */

loadTones();
updateMemoryStatus();

/*
###
### EVENTS
###
 */

playC3.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["C3"], 0);
        voice1.push("C3");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playCs3.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["Cs3"], 0);
        voice1.push("Cs3");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playD3.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["D3"], 0);
        voice1.push("D3");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playDs3.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["Ds3"], 0);
        voice1.push("Ds3");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playE3.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["E3"], 0);
        voice1.push("E3");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playF3.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["F3"], 0);
        voice1.push("F3");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playFs3.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["Fs3"], 0);
        voice1.push("Fs3");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playG3.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["G3"], 0);
        voice1.push("G3");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playGs3.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["Gs3"], 0);
        voice1.push("Gs3");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playA3.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["A3"], 0);
        voice1.push("A3");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playAs3.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["As3"], 0);
        voice1.push("As3");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playH3.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["H3"], 0);
        voice1.push("H3");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playC4.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["C4"], 0);
        voice1.push("C4");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playCs4.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["Cs4"], 0);
        voice1.push("Cs4");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playD4.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["D4"], 0);
        voice1.push("D4");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playDs4.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["Ds4"], 0);
        voice1.push("Ds4");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playE4.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["E4"], 0);
        voice1.push("E4");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playF4.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["F4"], 0);
        voice1.push("F4");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playFs4.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["Fs4"], 0);
        voice1.push("Fs4");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playG4.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["G4"], 0);
        voice1.push("G4");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playGs4.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["Gs4"], 0);
        voice1.push("Gs4");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playA4.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["A4"], 0);
        voice1.push("A4");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playAs4.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["As4"], 0);
        voice1.push("As4");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playH4.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["H4"], 0);
        voice1.push("H4");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playC5.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length < maxVoiceLength) {
        playTone(toneBuffers["C5"], 0);
        voice1.push("C5");
        updateVoiceStatus();
    } else {
        alert("The first voice can have only " + maxVoiceLength + " tones at most!");
    }
});

playVoice1.addEventListener("click", function() {
    if (context.state === 'suspended') {   // browser policy
        context.resume();
    }

    if (voice1.length > 0) {
        playVoice(voice1, 0);
    } else {
        alert("You don't have any notes in your first voice!");
    }
});

resetVoice1.addEventListener("click", function() {
    voice1 = [];
    voice2 = [];
    updateVoiceStatus();
});

undoVoice1.addEventListener("click", function() {
    voice1.pop();
    voice2 = [];    // we will generate a new one anyway
    updateVoiceStatus();
});

saveVoice1.addEventListener("click", function () {
    if (voice1.length > 0) {
        let name = prompt("Please type in a name for your melody:");
        let cleanName = name.replace(/<\/?[^>]+(>|$)/g, "");

        if (cleanName !== "") {

            storage.setItem(cleanName, voice1);
            updateMemoryStatus();
        } else {
            alert("You didn't type in a name!");
        }
    } else {
        alert("You don't have any notes to save!");
    }
});

loadVoice1.addEventListener("click", function () {
    let name = prompt("Please type in a name of your melody:");
    let cleanName = name.replace(/<\/?[^>]+(>|$)/g, "");

    if (cleanName !== "") {

        let voice1Raw = storage.getItem(cleanName);
        if (voice1Raw != null) {

            let voice1Split = voice1Raw.split(",");
            voice1 = Array.from(voice1Split);
            voice2 = [];
            updateVoiceStatus();
        } else {
            alert("There is no melody saved under that name!");
        }
    } else {
        alert("You didn't type in a name!");
    }
});

removeVoice1.addEventListener("click", function() {
   let name = prompt("Please type in a name of a melody to delete:");
    let cleanName = name.replace(/<\/?[^>]+(>|$)/g, "");

    if (cleanName !== "") {

        storage.removeItem(cleanName);
        updateMemoryStatus();
    } else {
        alert("You didn't type in a name!");
    }
});

playVoice2.addEventListener("click", function() {
    if (voice1.length > 0) {    // if there are no notes in the first voice, throw alert

        switch (intervalSelect.value) {

            case "upperDecima":
                transformVoice(16);    // major
                break;
            case "upperOctave":
                transformVoice(12);
                break;
            case "upperFifth":
                transformVoice(7);
                break;
            case "upperFourth":
                transformVoice(5);
                break;
            case "unison":
                voice2 = Array.from(voice1);    // deep copy
                break;
            case "lowerFourth":
                transformVoice(-5);
                break;
            case "lowerFifth":
                transformVoice(-7);
                break;
            case "lowerOctave":
                transformVoice(-12);
                break;
            case "lowerDecima":
                transformVoice(-15);    // minor
                break;
        }

        updateVoiceStatus();

        let shift = parseInt(shiftSelect.value);
        if (Number.isInteger(shift)
                && shift >= 0
                && shift <= 4) {

            playVoices(shift);
        } else {
            alert("The beat shift number has to be between 0 and 4!");
        }

    } else {
        alert("You don't have any notes in your first voice!");
    }
});

camden.addEventListener("click", function() {
    voice1 = ["G3","E3","C3","E3","G3","C4","E4","D4","C4","E3","Fs3","G3","G3","E4","D4","C4"];
    voice2 = [];
    updateVoiceStatus();
});